<?php
namespace Admin\Controller;
use Common\Controller\AdminBaseController;

/**
 * Class IndexController 后台默认控制器
 * @package Admin\Controller
 */
class IndexController extends AdminBaseController {

    /**
     * 后台默认首页
     */
    public function index(){

    }

}
