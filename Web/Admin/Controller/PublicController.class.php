<?php
namespace Admin\Controller;
use Think\Controller;

class PublicController extends Controller {

    public function index(){
        $this->redirect('login');
    }

    /**
     * 登录
     */
    public function login(){
        if(IS_POST){ $this->login_post();}
        if(is_login()){ $this->redirect('Index/index');}
        $this->display();
    }


    /**
     * 处理登录表单
     */
    private function login_post(){
        $verify = I('post.verify'); // 验证码
        $remember = I('remember',0,'intval');

        // 检测验证码
        if(empty($verify) || !$this->check_verify($verify)) $this->error('验证码填写错误');

        $model = D('Admin');
        $data = $model->create($_POST,4);
        if(!$data) $this->error($model->getError());

        $admin = $model->login($data['email'],$data['password'],$remember);
        if(!$admin) $this->error($model->getError());

        $this->success('登录成功',U('Index/index'));
    }


    /**
     * 退出登录
     */
    public function logout(){
        if(is_login()){
            session(null);
            cookie('admin_token',null);
            $this->redirect('Index/index');
        }
    }

    /**
     * 产生验证码
     */
    public function verify(){
        $config=array(
            'imageW'=>'320px',
            // 'imageH'=>'50px',
            'length'=>1,
            'fontSize'=>30,
            'useCurve'=>false,
            'useNoise'=>true,
            'fontttf' => '2.ttf',
            'codeSet' => '23456789ABCDEFGHIJKLMNPQRSTUVWXYZ',
        );
        $verify = new \Think\Verify($config);
        $verify->entry(1);
    }

    /**
     * 验证验证码
     * @param $code
     * @return bool
     */
    private function check_verify($code){
        $Verify = new \Think\Verify();
        if(!$Verify->check($code, 1)) return false;
        return true;
    }


    /**
     * 空方法
     */
    public function _empty(){
        $this->redirect('login');
    }


    public function test(){
        echo 1;
    }
}
