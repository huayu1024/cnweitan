<?php
namespace Admin\Model;
use Common\Model\CommonModel;

class AdminModel extends CommonModel {
    protected $_validate = array(
        // 登录自动验证
        array('email','email','请正确填写邮箱地址',self::MUST_VALIDATE,'regex',4),
        array('password', '6,30', '密码长度不符,请保持在6-30个字符之内',self::MUST_VALIDATE,'length',4),
    );


    /**
     * 后台登录处理模型
     * @param     $email 用户名
     * @param     $password 密码
     * @param int $remember 是否记住登录
     */
    public function login($email,$password,$remember=0){
        // 登陆错误_检测
        $r = $this->login_error_check();

        if($r !== TRUE) {
            $this->error = $r;
            return false;
        }

        $admin = $this->where(array('email'=>$email))->find();
        if(!$admin){
            action_log('login',1);
            $this->error = '账号不存在';
            return false;
        }

        if(!$admin['status']){
            action_log('login',2);
            $this->error='账号已被禁用';
            return false;
        };

        $pass = $this->get_password($password,$admin['encrypt']);
        if($pass != $admin['password']){
            action_log('login',3);
            $this->error = '密码错误';
            return false;
        }

        // 添加认证
        $this->admin_info($admin);

        // 记住登录
        if($remember) $this->admin_token('add',$admin);

        // 成功登录
        action_log('login',0,$admin['id']);
        return $admin;
    }


    /**
     * 添加session认证
     * @param array $admin 用户登录的信息
     */
    public function admin_info($admin=array()){

        // 单点session认证
        if($admin['session_id'] != session_id() ){
            $this->where("id={$admin['id']}")->setField('session_id',session_id());
            session_id($admin['session_id']);
            session_destroy();
            session_start();
        }

        $admin_info = array(
            'id'=>$admin['id'],
            'mobile'=>$admin['mobile'],
            'email'=>$admin['email'],
        );
        session('admin_info',$admin_info);
    }


    /**
     * 添加cookie持久认证
     * @param string $type 类型,add/check
     * @param array  $admin 用户信息
     * @return bool
     */
    public function admin_token($type='check',$admin=array()){
        switch ($type) {
            case 'add':
                if(empty($admin)) return false;
                $token = array();
                $token['token_time'] = time() + 3600 * 24 * 7;
                $token['token'] = md5($admin['email'].$token['token_time']);
                if( $this->where("id={$admin['id']}")->save($token) ){
                    cookie('admin_token', $token['token'], 3600 * 24 * 7);
                }else{
                    return false;
                }
                break;
            case 'check':
                $token = cookie('admin_token');
                if(!$token){ return false; }
                // 强制清除无效/过期cookie持久认证
                $admin = $this->where("token='{$token}'")->find();
                if( !$admin || $admin['token_time'] < time() ){
                    cookie('admin_token',null);
                    return false;
                }

                // 重新添加session认证
                $this->admin_info($admin);
                return $admin['id'];
                break;
        }
        return true;
    }


    // 登陆错误_检测
    private function login_error_check(){
        $map['uid'] = 0;
        $map['log_action'] = 'login';
        $map['log_time_format'] =  date('YmdH',NOW_TIME);
        $map['log_ip'] = get_client_ip();
        $count = M('AdminLog')->where($map)->count();
        if($count > 5){
            return '<b style="color:#f0ad4e">'.$map['log_ip'].'</b><br />登录过于频繁,1小时后尝试！';
        }else{
            return TRUE;
        }
    }


    /**
     * 自动验证或对比密码
     * @param string $str 密码
     * @param string $encrypt 密码密匙
     * @return bool|string
     */
    public function get_password($str='',$encrypt=''){
        if(empty($str)) return false;
        if(empty($encrypt)){
            $encrypt = \Org\Util\String::randString(6);
        }
        return md5(sha1($str) . $encrypt);
    }

}