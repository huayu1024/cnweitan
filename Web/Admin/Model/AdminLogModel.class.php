<?php
namespace Admin\Model;
use Common\Model\CommonModel;

/**
 * Class AdminLogModel 后台日志模型
 * @package Admin\Model
 */
class AdminLogModel extends CommonModel {
    // 自动完成
    protected $_auto = array (
        array('log_ip','get_client_ip',self::MODEL_INSERT,'function'),
        array('log_time','time',self::MODEL_INSERT,'function'),
        array('log_time_format','date',self::MODEL_INSERT,'function',array('YmdH')),
    );

    // 替换模板
    private $record_tpl = array(
        'login'=>array(
            '[#email#]登陆系统',
            'IP为[#action_ip#]的用户使用账号[#email#]密码[#password#]尝试登陆系统',
            '禁用账号[#email#]使用密码[#password#]尝试登陆系统',
            '账号[#email#]使用错误密码[#password#]尝试登陆系统',
        ),
    );


    /**
     * 增加日志
     * @param string $action 方法
     * @param int    $type  类型
     * @param int    $uid   用户ID
     * @return bool
     */
    public function log($action='',$type=0,$uid=0){
        $data = $this->create();
        $data['uid'] = defined(UID) ? UID : $uid;
        $data['log_type'] = $type;
        $data['log_action'] = $action;
        $data['action_ip'] =  get_client_ip();
        $data['log_record'] = $this->get_record($data,$type);

        $r = $this->add($data);
        if($r){
            return true;
        }else{
            $this->error='添加操作记录失败';
            return false;
        }
    }


    /**
     * 文字生成
     * @param array  $data 默认收集的数据
     * @param string $type 类型
     * @return mixed
     */
    private function get_record($data=array(),$type=''){
        $post = I('post.');
        if(!empty($post)) $data = array_merge($post,$data);
        // 获取记录模板
        $tpl = $this->record_tpl[$data['log_action']][$type];
        // 获取待替换字符的数组
        preg_match_all('/#(\w+)#/',$tpl,$wait);
        // 构建正则替换参数
        for ($i=0; $i < count($wait[0]); $i++) {
            $patterns[] = '/'.$wait[0][$i].'/';
            $replacements[] = $data[$wait[1][$i]];
        }
        $result = preg_replace($patterns, $replacements, $tpl);
        return $result;
    }
}