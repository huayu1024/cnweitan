<?php

/**
 * 检测登录状态
 * @return bool|int
 */
function is_login(){
    if($admin = session('admin_info')){
        $uid = $admin['id'];
    }elseif($token = cookie('admin_token')){
        $uid = D('Admin')->admin_token('check');
    }else{
        return false;
    }
    return intval($uid);
}


/**
 * 通过动态配置开启关闭日志功能
 * @param string $action 方法名
 * @param int    $type 类型,关联模板变量
 * @param int    $uid 用户ID
 * @return bool
 */
function action_log($action='',$type=0,$uid=0){
    $log_status=array(
        'login'=>true,
    );
    if($log_status[$action]){
        D('AdminLog')->log($action,$type,$uid);
    }
    // 错误操作相关人员提醒
    if(!$type){

    }
    return true;
}






