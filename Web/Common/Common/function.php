<?php

/**
 * 字符串转数组
 * @param string $string 需要转换的字符串
 * @return array|bool 字符串不为空，则返回数组;
 */
function str_to_arr($string=''){
    if(empty($string)) return false;
    $array = preg_split('/[,;\r\n]+/', trim($string, ",;\r\n"));
    if(strpos($string,':')){
        $value  = array();
        foreach ($array as $val) {
            $_i = strpos($val,':');
            $_k = substr($val, 0,$_i);
            $_v = substr($val, $_i+1);
            $value[$_k] = $_v;
        }
        return $value;
    }else{
        return $array;
    }
}

