<?php
namespace Common\Controller;
use Think\Controller;

/**
 * Class BaseController 全站基础控制器
 * 初始化一些全站通用的信息;
 * @package Common\Controller
 */
class BaseController extends Controller {

    /**
     * 全站初始化
     */
    public function _initialize(){
        self::init_Config();
        if(C('SITE_STATUS') == -1){// -1 全站关闭,相关介绍在配置中查看

        }
    }


    /**
     * 加载 Common 动态配置
     */
    protected function init_Config($module='Common'){
        $config_Name = $module.'Config';
        $common_config = F($config_Name);
        if(!$common_config){
            $common_config = D('Config')->get_Config($module);
            F($config_Name,$common_config);
        }
        C($common_config);
    }
}


