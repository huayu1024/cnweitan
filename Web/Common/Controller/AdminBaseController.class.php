<?php
namespace Common\Controller;

class AdminBaseController extends BaseController {

    /**
     * 后台初始化
     */
    public function _initialize(){
        parent::_initialize();
        parent::init_Config('Admin');   // 初始化后台配置

        if(C('SITE_STATUS') == -2){// -2 后台关闭,相关介绍在配置中查看

        }

        /**** 权限认证 ****/
        $uid = is_login();
        if(!$uid) $this->redirect('Public/login');
        define('UID',$uid);


    }

}
