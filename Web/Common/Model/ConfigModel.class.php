<?php
namespace Common\Model;

class ConfigModel extends CommonModel{

    /**
     * 获取指定模块的配置文件
     * @param string $module 模块名称
     * @return array 指定模块下的配置信息
     */
    public function get_Config($module='Common'){
        $map['status'] = array('eq',1);
        $map['module'] = array('eq',$module);

        $list = $this->field('name,value,type')->where($map)->select();
        $config = array();
        foreach ($list as $v) {
            switch ($v['type']) {
                case 11: // 数组
                    $config[$v['name']] = str_to_arr($v['value']);
                    break;
                case 9: // 布尔
                    $config[$v['name']] = (bool)$v['value'];
                    break;
                default: // 字符串
                    $config[$v['name']] = $v['value'];
                    break;
            }
        }
        return $config;
    }




}