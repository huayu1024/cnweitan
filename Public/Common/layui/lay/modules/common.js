layui.define(['layer'], function(exports){
    var layer = layui.layer;

    /**
     * 后台登录界面的提示
     */
    $('.ajax-login-form').on('submit', function() {
        var self = $(this);
        var email = self.find("#email").find('.email');
        if(email.val() == ''){
            layer.tips('请填写邮箱!', '#email', {
                tips: [1, '#c00']
            });
            return ;
        }

        var password = self.find("#password").find('.password');
        if(password.val() == ''){
            layer.tips('请填写密码!', '#password', {
                tips: [1, '#c00']
            });
            return ;
        }

        var code = self.find("#Code").find('.Code');
        if(code.val() == ''){
            layer.tips('请填写验证码!', '#Code', {
                tips: [1, '#c00']
            });
            return ;
        }
        $.post(self.attr("action"), self.serialize(), success, "json");
        layer.closeAll();
        var loadIndex = layer.load(2,{shade: [0.1, '#676767']});
        function success(data){
            if(data.status){
                self.find(".btn-flat").addClass('btn-success').text('登录中...');
                layer.close(loadIndex);
                layer.msg('登录中...',{shade: [0.3, '#676767']});
                setTimeout(function() {
                    window.location.href = data.url;
                }, 3000);
            } else {
                layer.close(loadIndex);
                layer.msg(data.info,{area: '200px'})
                $("#verify").click();//刷新验证码
            }
        }

        return false;
    });



    exports('common', {});

});




/**
 * 刷新验证码
 */
var verifyimg = $("#verify").attr("src");
$("#verify").click(function(){
   if( verifyimg.indexOf('?')>0){
       $("#verify").attr("src", verifyimg+'&random='+Math.random());
   }else{
       $("#verify").attr("src", verifyimg.replace(/\?.*$/,'')+'?'+Math.random());
   }
});

$(function () {
    // 禁止登录表单提交
    $("#LoginForm").submit(function () {
        return false;
    });

    // input表单美化
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
});